jdb
===

jdb is a lightweight object-relational-mapper (ORM). It is designed to be easy
to use but also small and fast.

Development
===========

This is the stable `release/1.0`-branch!
