package de.tforge.jdbtest;

import de.tforge.jdb.DatabaseException;
import de.tforge.jdb.DatabaseManager;
import de.tforge.jdb.mysql.MySqlDatabaseFactory;
import org.joda.time.DateTime;

import static de.tforge.jdb.OperatorBuilders.*;

/**
 * Created by tim on 22.02.17.
 */
public class TestMain {
    public static void main(String[] args) {
        MySqlDatabaseFactory.setDatabase("jdbc:mariadb://tforge-root.tfl:3306/btcdev?autoReconnect=true", "btcdev", "btcdev");
        DatabaseManager.setFactory(MySqlDatabaseFactory.getInstance());

        try {
            // Print some deals
            Deal
                    .where(Deal.TIME, greaterThan(new DateTime(2017, 1, 3, 0, 0, 0)))
                    .where(Deal.TIME, lessThan(new DateTime(2019, 1, 4, 15, 15, 0)))
                    .get()
                    .forEach(System.out::println);



            // Update the third deal time.
            Deal.skip(2).first().setTime(DateTime.now()).save();



            // Create a new deal.
            /*Deal d = new Deal();
            d.setAmount(13.37);
            d.setPrice(4.2);
            d.setTime(DateTime.now());
            d.setIsBuy(false);
            d.setMarketplaceID(1);
            d.save();*/

            System.out.printf("Deals: %d\n", Deal.getQueryBuilder().count());

            Deal d = new Deal(42);

            Deal deal = new Deal(10);
            deal.setPrice(42713);
            deal.delete();
        }
        catch (DatabaseException e) {
            // Oh noes!
            e.printStackTrace();
        }
    }
}
