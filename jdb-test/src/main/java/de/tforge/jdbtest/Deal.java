package de.tforge.jdbtest;

import de.tforge.jdb.*;
import org.joda.time.DateTime;

import java.util.AbstractMap.SimpleEntry;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static de.tforge.jdb.OperatorBuilders.equalTo;

/**
 * Created by tim on 24.02.17.
 */
public class Deal extends Model {
    public final static String DEAL_ID = "dealID";
    public final static String MARKETPLACE_ID = "marketplaceID";
    public final static String TIME = "time";
    public final static String IS_BUY = "isBuy";
    public final static String PRICE = "price";
    public final static String AMOUNT = "amount";

    public static QueryBuilder<Deal> where(String column, Integer value) {
        return where(column, equalTo(value));
    }

    public static QueryBuilder<Deal> where(String column, Long value) {
        return where(column, equalTo(value));
    }

    public static QueryBuilder<Deal> where(String column, Float value) {
        return where(column, equalTo(value));
    }

    public static QueryBuilder<Deal> where(String column, Double value) {
        return where(column, equalTo(value));
    }

    public static QueryBuilder<Deal> where(String column, Boolean value) {
        return where(column, equalTo(value));
    }

    public static QueryBuilder<Deal> where(String column, DateTime value) {
        return where(column, equalTo(value));
    }

    public static QueryBuilder<Deal> where(String column, String value) {
        return where(column, equalTo(value));
    }

    public static QueryBuilder<Deal> where(String column, Operator op) {
        return getQueryBuilder().where(column, op);
    }


    public static QueryBuilder<Deal> order(String column) {
        return getQueryBuilder().order(column);
    }

    public static QueryBuilder<Deal> order(String column, OrderDirection direction) {
        return getQueryBuilder().order(column, direction);
    }


    public static QueryBuilder<Deal> skip(int count) {
        return getQueryBuilder().skip(count);
    }

    public static QueryBuilder<Deal> take(int count) {
        return getQueryBuilder().take(count);
    }

    public static Deal first() throws DatabaseException {
        return getQueryBuilder().first();
    }

    public static long count() throws DatabaseException {
        return getQueryBuilder().count();
    }


    public static QueryBuilder<Deal> getQueryBuilder() {
        return DatabaseManager.createQueryBuilder(Deal.class, getTableName());
    }

    public static String getTableName() {
        return "btc_deals";
    }

    private long dealID;
    private long marketplaceID;
    private DateTime time;
    private boolean isBuy;
    private double price;
    private double amount;

    public Deal() {
    }

    public Deal(long dealID) throws DatabaseException {
        Result result = DatabaseManager.createQueryBuilder(Result.class, getTableName())
                .where(DEAL_ID, dealID)
                .first();

        if(result == null) {
            throw new DatabaseException(String.format("Unable to find Deal #%s", dealID));
        }

        adaptResult(result);
    }

    @Override
    public void adaptResult(Result r) {
        if (this.exists) {
            return;
        }

        this.exists = true;
        this.dealID = (long) r.getColumn("dealID").getValue();
        this.marketplaceID = (long) r.getColumn("marketplaceID").getValue();
        this.time = (DateTime) r.getColumn("time").getValue();
        this.isBuy = (boolean) r.getColumn("isBuy").getValue();
        this.price = (double) r.getColumn("price").getValue();
        this.amount = (double) r.getColumn("amount").getValue();
    }

    @Override
    public Map<String, Object> getAttributes() {
        return Collections.unmodifiableMap(Stream.of(
                new SimpleEntry<>("dealID", dealID),
                new SimpleEntry<>("marketplaceID", marketplaceID),
                new SimpleEntry<>("time", time),
                new SimpleEntry<>("isBuy", isBuy),
                new SimpleEntry<>("price", price),
                new SimpleEntry<>("amount", amount)
        ).collect(Collectors.toMap(SimpleEntry::getKey, SimpleEntry::getValue)));
    }

    @Override
    public void save() throws DatabaseException {
        Object id = save(getTableName());

        if(id != null) {
            dealID = (long) id;
        }
    }

    @Override
    public void delete() throws DatabaseException {
        delete(getTableName());
    }

    @Override
    protected String getIdColumnName() {
        return DEAL_ID;
    }

    public long getDealID() {
        return dealID;
    }

    public long getMarketplaceID() {
        return marketplaceID;
    }

    public Deal setMarketplaceID(long marketplaceID) {
        this.marketplaceID = marketplaceID;
        this.isDirty = true;
        return this;
    }

    public DateTime getTime() {
        return time;
    }

    public Deal setTime(DateTime time) {
        this.time = time;
        this.isDirty = true;
        return this;
    }

    public boolean getIsBuy() {
        return isBuy;
    }

    public Deal setIsBuy(Boolean isBuy) {
        this.isBuy = isBuy;
        this.isDirty = true;
        return this;
    }

    public double getPrice() {
        return price;
    }

    public Deal setPrice(double price) {
        this.price = price;
        this.isDirty = true;
        return this;
    }

    public double getAmount() {
        return amount;
    }

    public Deal setAmount(double amount) {
        this.amount = amount;
        this.isDirty = true;
        return this;
    }

    @Override
    public String toString() {
        return "{\n" +
                String.format("    dealID -> %s\n", dealID) +
                String.format("    marketplaceID -> %s\n", marketplaceID) +
                String.format("    time -> %s\n", time) +
                String.format("    isBuy -> %s\n", isBuy) +
                String.format("    price -> %s\n", price) +
                String.format("    amount -> %s\n", amount) +
                "}";
    }
}
