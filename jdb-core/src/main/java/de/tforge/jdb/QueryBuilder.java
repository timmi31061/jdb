package de.tforge.jdb;

import org.joda.time.DateTime;

import java.util.stream.Stream;

/**
 * Created by tim on 22.02.17.
 */
public interface QueryBuilder<T> {
    QueryBuilder<T> where(String column, Integer value);
    QueryBuilder<T> where(String column, Long value);
    QueryBuilder<T> where(String column, Float value);
    QueryBuilder<T> where(String column, Double value);
    QueryBuilder<T> where(String column, Boolean value);
    QueryBuilder<T> where(String column, DateTime value);
    QueryBuilder<T> where(String column, String value);
    QueryBuilder<T> where(String column, Operator op);

    QueryBuilder<T> order(String column);
    QueryBuilder<T> order(String column, OrderDirection direction);

    QueryBuilder<T> skip(long count);
    QueryBuilder<T> take(long count);

    T first() throws DatabaseException;
    Stream<T> get() throws DatabaseException;
    long count() throws DatabaseException;
}
