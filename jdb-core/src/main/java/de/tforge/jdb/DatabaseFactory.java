package de.tforge.jdb;

/**
 * Created by tim on 22.02.17.
 */
public interface DatabaseFactory {
    <T extends Model> QueryBuilder<T> createQueryBuilder(Class<T> modelClass, String tableName);

    OperatorFactory getOperatorFactory();
    TypeConverter getTypeConverter();
    ModelPersister getModelPersister();
}
