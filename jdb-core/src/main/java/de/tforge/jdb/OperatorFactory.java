package de.tforge.jdb;

import org.joda.time.DateTime;

/**
 * Created by tim on 22.02.17.
 */
public interface OperatorFactory {
    Operator equalTo(Integer val);
    Operator equalTo(Long val);
    Operator equalTo(Float val);
    Operator equalTo(Double val);
    Operator equalTo(Boolean val);
    Operator equalTo(DateTime val);
    Operator equalTo(String val);

    Operator greaterThan(Integer val);
    Operator greaterThan(Long val);
    Operator greaterThan(Float val);
    Operator greaterThan(Double val);
    Operator greaterThan(DateTime val);

    Operator lessThan(Integer val);
    Operator lessThan(Long val);
    Operator lessThan(Float val);
    Operator lessThan(Double val);
    Operator lessThan(DateTime val);

    Operator inArray(Object[] vals);

    Operator not(Operator op);
}
