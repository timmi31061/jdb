package de.tforge.jdb.mysql;

import de.tforge.jdb.TypeConverter;
import org.joda.time.DateTime;

import java.sql.Timestamp;

/**
 * Created by tim on 25.02.17.
 */
public class MySqlTypeConverter implements TypeConverter {
    @Override
    public Object convertFromDB(Object value) {
        if(value instanceof Timestamp) {
            return new DateTime(((Timestamp) value).getTime());
        }

        return value;
    }

    @Override
    public Object convertToDB(Object value) {
        if(value instanceof DateTime) {
            return new Timestamp(((DateTime) value).getMillis());
        }

        return value;
    }
}
