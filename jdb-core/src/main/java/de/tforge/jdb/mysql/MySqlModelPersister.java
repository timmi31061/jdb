package de.tforge.jdb.mysql;

import de.tforge.jdb.DatabaseException;
import de.tforge.jdb.DatabaseManager;
import de.tforge.jdb.ModelPersister;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by tim on 26.02.17.
 */
public class MySqlModelPersister implements ModelPersister {
    @Override
    public Object persistNew(String tableName, String idColumn, Map<String, Object> columns) throws DatabaseException {
        String sql = "INSERT INTO %s (%s) VALUES (%s)";

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < columns.size() - 1; i++) {
            builder.append(",?");
        }

        List<String> cols = columns.keySet().stream()
                .filter(s -> !s.equals(idColumn))
                .collect(Collectors.toList());

        sql = String.format(sql,
                tableName,
                String.join(",", cols),
                builder.toString().substring(1)
        );

        try (PreparedStatement st = MySqlDatabaseFactory.getInstance().prepare(sql, Statement.RETURN_GENERATED_KEYS)) {
            for (int i = 0; i < cols.size(); i++) {
                Object column = columns.get(cols.get(i));
                st.setObject(i + 1, DatabaseManager.getTypeConverter().convertToDB(column));
            }

            if (st.executeUpdate() == 0) {
                throw new DatabaseException(String.format("No rows affected by insert operation. Query: %s", sql));
            }

            try (ResultSet rs = st.getGeneratedKeys()) {
                if (rs.next()) {
                    return rs.getObject(1);
                }
                else {
                    throw new DatabaseException(String.format("No ID obtained by insert operation. Query: %s", sql));
                }
            }
        }
        catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public void persistExisting(String tableName, String idColumn, Map<String, Object> columns) throws DatabaseException {
        String sql = "UPDATE %s SET %s WHERE %s = ?";

        List<String> cols = columns.keySet().stream()
                .filter(s -> !s.equals(idColumn))
                .collect(Collectors.toList());

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < columns.size() - 1; i++) {
            builder.append(",`");
            builder.append(cols.get(i));
            builder.append("`=?");
        }

        sql = String.format(sql,
                tableName,
                builder.toString().substring(1),
                idColumn
        );

        try (PreparedStatement st = MySqlDatabaseFactory.getInstance().prepare(sql)) {
            int i;
            for (i = 0; i < cols.size(); i++) {
                Object column = columns.get(cols.get(i));
                st.setObject(i + 1, DatabaseManager.getTypeConverter().convertToDB(column));
            }

            st.setObject(i + 1, columns.get(idColumn));

            if (st.executeUpdate() == 0) {
                throw new DatabaseException(String.format("No rows affected by update operation. Query: %s", sql));
            }
        }
        catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public void delete(String tableName, String idColumn, Map<String, Object> columns) throws DatabaseException {
        String sql = "DELETE FROM %s WHERE %s = ?";
        sql = String.format(sql, tableName, idColumn);

        try (PreparedStatement st = MySqlDatabaseFactory.getInstance().prepare(sql)) {
            st.setObject(1, columns.get(idColumn));

            if (st.executeUpdate() == 0) {
                throw new DatabaseException(String.format("No rows affected by delete operation. Query: %s", sql));
            }
        }
        catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }
}
