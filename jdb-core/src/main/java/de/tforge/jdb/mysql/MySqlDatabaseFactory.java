package de.tforge.jdb.mysql;

import de.tforge.jdb.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by tim on 22.02.17.
 */
public class MySqlDatabaseFactory implements DatabaseFactory {
    private static MySqlDatabaseFactory instance = null;

    private static String uri;
    private static String user;
    private static String password;

    public static MySqlDatabaseFactory getInstance() {
        if (instance == null) {
            instance = new MySqlDatabaseFactory();
        }
        return instance;
    }

    public static void setDatabase(String uri, String user, String password) {
        MySqlDatabaseFactory.uri = uri;
        MySqlDatabaseFactory.user = user;
        MySqlDatabaseFactory.password = password;
    }

    private final MySqlOperatorFactory operatorFactory = new MySqlOperatorFactory();
    private final MySqlTypeConverter typeConverter = new MySqlTypeConverter();
    private final MySqlModelPersister modelPersister = new MySqlModelPersister();
    private Connection connection;

    private MySqlDatabaseFactory() {
        try {
            Class.forName("org.mariadb.jdbc.Driver");
            connection = DriverManager.getConnection(uri, user, password);
        }
        catch (Exception e) {
            throw new Error(e);
        }
    }

    @Override
    public <T extends Model> QueryBuilder<T> createQueryBuilder(Class<T> modelClass, String tableName) {
        return new MySqlQueryBuilder<>(modelClass, tableName);
    }

    @Override
    public OperatorFactory getOperatorFactory() {
        return operatorFactory;
    }

    @Override
    public TypeConverter getTypeConverter() {
        return typeConverter;
    }

    @Override
    public ModelPersister getModelPersister() {
        return modelPersister;
    }

    public PreparedStatement prepare(String sql) throws SQLException {
        return connection.prepareStatement(sql);
    }

    public PreparedStatement prepare(String sql, int statementOptions) throws SQLException {
        return connection.prepareStatement(sql, statementOptions);
    }
}
