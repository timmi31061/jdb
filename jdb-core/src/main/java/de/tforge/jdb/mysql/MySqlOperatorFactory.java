package de.tforge.jdb.mysql;

import de.tforge.jdb.DatabaseManager;
import de.tforge.jdb.Operator;
import de.tforge.jdb.OperatorFactory;
import org.joda.time.DateTime;

/**
 * Created by tim on 22.02.17.
 */
public class MySqlOperatorFactory implements OperatorFactory {
    @Override
    public Operator equalTo(Integer val) {
        return new MySqlOperator<>("=", val);
    }

    @Override
    public Operator equalTo(Long val) {
        return new MySqlOperator<>("=", val);
    }

    @Override
    public Operator equalTo(Float val) {
        return new MySqlOperator<>("=", val);
    }

    @Override
    public Operator equalTo(Double val) {
        return new MySqlOperator<>("=", val);
    }

    @Override
    public Operator equalTo(Boolean val) {
        return new MySqlOperator<>("=", val ? 1 : 0);
    }

    @Override
    public Operator equalTo(DateTime val) {
        return new MySqlOperator<>("=", DatabaseManager.getTypeConverter().convertToDB(val));
    }

    @Override
    public Operator equalTo(String val) {
        return new MySqlOperator<>("=", val);
    }



    @Override
    public Operator greaterThan(Integer val) {
        return new MySqlOperator<>(">", val);
    }

    @Override
    public Operator greaterThan(Long val) {
        return new MySqlOperator<>(">", val);
    }

    @Override
    public Operator greaterThan(Float val) {
        return new MySqlOperator<>(">", val);
    }

    @Override
    public Operator greaterThan(Double val) {
        return new MySqlOperator<>(">", val);
    }

    @Override
    public Operator greaterThan(DateTime val) {
        return new MySqlOperator<>(">", DatabaseManager.getTypeConverter().convertToDB(val));
    }



    @Override
    public Operator lessThan(Integer val) {
        return new MySqlOperator<>("<", val);
    }

    @Override
    public Operator lessThan(Long val) {
        return new MySqlOperator<>("<", val);
    }

    @Override
    public Operator lessThan(Float val) {
        return new MySqlOperator<>("<", val);
    }

    @Override
    public Operator lessThan(Double val) {
        return new MySqlOperator<>("<", val);
    }

    @Override
    public Operator lessThan(DateTime val) {
        return new MySqlOperator<>("<", DatabaseManager.getTypeConverter().convertToDB(val));
    }



    @Override
    public Operator inArray(Object[] vals) {
        return new MySqlOperator<>("IN", vals);
    }

    @Override
    public Operator not(Operator op) {
        return new MySqlOperator<>("NOT", op);
    }
}
