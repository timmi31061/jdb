package de.tforge.jdb.mysql;

import de.tforge.jdb.*;
import org.joda.time.DateTime;

import static de.tforge.jdb.OperatorBuilders.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by tim on 22.02.17.
 */
public class MySqlQueryBuilder<T extends Model> implements QueryBuilder<T> {
    private final String tableName;
    private final Class<T> modelClass;

    private LinkedList<MySqlWhereStatement> whereStatements = new LinkedList<>();
    private long skip;
    private long take;
    private String orderBy = null;
    private OrderDirection orderDirection;

    public MySqlQueryBuilder(Class<T> modelClass, String tableName) {
        this.tableName = tableName;
        this.modelClass = modelClass;
    }

    @Override
    public QueryBuilder<T> where(String column, Integer value) {
        return where(column, equalTo(value));
    }

    @Override
    public QueryBuilder<T> where(String column, Long value) {
        return where(column, equalTo(value));
    }

    @Override
    public QueryBuilder<T> where(String column, Float value) {
        return where(column, equalTo(value));
    }

    @Override
    public QueryBuilder<T> where(String column, Double value) {
        return where(column, equalTo(value));
    }

    @Override
    public QueryBuilder<T> where(String column, Boolean value) {
        return where(column, equalTo(value));
    }

    @Override
    public QueryBuilder<T> where(String column, DateTime value) {
        return where(column, equalTo(value));
    }

    @Override
    public QueryBuilder<T> where(String column, String value) {
        return where(column, equalTo(value));
    }

    @Override
    public QueryBuilder<T> where(String column, Operator op) {
        if (op instanceof MySqlOperator) {
            whereStatements.add(new MySqlWhereStatement(column, (MySqlOperator) op));
        }
        else {
            throw new IllegalArgumentException("op must be an instance of MySqlOperator.");
        }

        return this;
    }

    @Override
    public QueryBuilder<T> order(String column) {
        return order(column, OrderDirection.ASC);
    }

    @Override
    public QueryBuilder<T> order(String column, OrderDirection direction) {
        orderBy = column;
        orderDirection = direction;
        return this;
    }

    @Override
    public QueryBuilder<T> skip(long count) {
        skip = count;
        return this;
    }

    @Override
    public QueryBuilder<T> take(long count) {
        take = count;
        return this;
    }

    @Override
    public T first() throws DatabaseException {
        return take(1).get().findFirst().orElse(null);
    }

    @Override
    public Stream<T> get() throws DatabaseException {
        String sql = buildSql();

        LinkedList<T> results = new LinkedList<>();

        try (PreparedStatement st = prepareAndBind(sql)) {
            try (ResultSet rs = st.executeQuery()) {
                ResultSetMetaData meta = rs.getMetaData();
                int columnCount = meta.getColumnCount();

                List<String> labels = new LinkedList<>();
                for (int i = 1; i <= columnCount; i++) {
                    labels.add(meta.getColumnLabel(i));
                }

                T model = null;
                while (rs.next()) {
                    if (model == null || !(model instanceof Result)) {
                        try {
                            model = modelClass.newInstance();
                        }
                        catch (Exception e) {
                            throw new DatabaseException(e);
                        }
                    }

                    Result r = new Result();

                    for (int i = 0; i < labels.size(); i++) {
                        String type = meta.getColumnTypeName(i + 1);
                        Object value = rs.getObject(i + 1);

                        r.setColumn(labels.get(i), r.new Entry(type, value));
                    }

                    if (model instanceof Result) {
                        results.add((T) r);
                    }
                    else {
                        model.adaptResult(r);
                        results.add(model);
                    }
                }
            }
        }
        catch (SQLException e) {
            throw new DatabaseException(e);
        }

        return results.stream();
    }

    @Override
    public long count() throws DatabaseException {
        String sql = "SELECT COUNT(*) AS `count` FROM (%s) AS count_%s";
        sql = String.format(sql, buildSql(), tableName);

        try(PreparedStatement st = prepareAndBind(sql)) {
            try (ResultSet rs = st.executeQuery()) {
                if(rs.next()) {
                    return rs.getLong(1);
                }
                else {
                    throw new DatabaseException(String.format("No rows returned. Query: %s", sql));
                }
            }
        }
        catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    private String buildSql() {
        String sql = "SELECT * FROM `" + tableName + "`";

        if (whereStatements.size() > 0) {
            sql += " WHERE";

            boolean isFirst = true;
            for (MySqlWhereStatement st : whereStatements) {
                // The statements are chained with AND operators
                if (!isFirst) {
                    sql += " AND";
                }

                // Sub-operators need to be handled specially, like NOT(`username` = 'John Doe')
                if (st.getOperator().getValue() instanceof Operator) {
                    sql += " " + st.getOperator().getOperator() + " (`" + st.getColumn() + "` " + st.getOperator().getValue().toString() + ")";
                }
                else {
                    sql += " `" + st.getColumn() + "` " + st.getOperator().toString();
                }
                isFirst = false;
            }
        }

        if (orderBy != null) {
            sql += " ORDER BY `" + orderBy + "` " + orderDirection.toString();
        }

        if (take > 0) {
            sql += " LIMIT " + skip + ", " + take;
        }

        return sql;
    }

    private PreparedStatement prepareAndBind(String sql) throws DatabaseException {
        PreparedStatement st = null;
        try {
            st = MySqlDatabaseFactory.getInstance().prepare(sql);
            int index = 0;
            for (MySqlWhereStatement where : whereStatements) {
                index = where.getOperator().bindParams(index, st);
            }
        }
        catch (SQLException e) {
            if(st != null)  {
                try {
                    st.close();
                }
                catch (SQLException ignored) {
                }
            }

            throw new DatabaseException(e);
        }

        return st;
    }
}
