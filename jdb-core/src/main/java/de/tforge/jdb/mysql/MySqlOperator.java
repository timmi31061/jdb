package de.tforge.jdb.mysql;

import de.tforge.jdb.Operator;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by tim on 22.02.17.
 */
public class MySqlOperator<T> implements Operator {
    private final String operator;
    private final T value;

    public MySqlOperator(String operator, T value) {
        this.operator = operator;
        this.value = value;
    }

    public String getOperator() {
        return operator;
    }

    public T getValue() {
        return value;
    }

    public int bindParams(int startingIndex, PreparedStatement statement) throws SQLException {
        if(value instanceof MySqlOperator) {
            return ((MySqlOperator) value).bindParams(startingIndex, statement);
        }
        if (value instanceof Object[]) {
            Object[] array = (Object[]) value;
            for (Object anArray : array) {
                statement.setObject(++startingIndex, anArray);
            }
        }
        else {
            statement.setObject(++startingIndex, value);
        }

        return startingIndex;
    }

    @Override
    public String toString() {
        if (value instanceof MySqlOperator) {
            return operator + " " + value.toString();
        }

        if (value instanceof Object[]) {
            Object[] array = (Object[]) value;
            StringBuilder builder = new StringBuilder();

            for (Object i : array) {
                builder.append(",?");
            }

            return operator + " (" + builder.toString().substring(1) + ")";
        }

        return operator + " ?";
    }
}
