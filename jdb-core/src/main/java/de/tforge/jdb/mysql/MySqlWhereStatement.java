package de.tforge.jdb.mysql;

/**
 * Created by tim on 22.02.17.
 */
public class MySqlWhereStatement {
    private final String column;
    private final MySqlOperator operator;

    public MySqlWhereStatement(String column, MySqlOperator operator) {
        this.column = column;
        this.operator = operator;
    }

    public String getColumn() {
        return column;
    }

    public MySqlOperator getOperator() {
        return operator;
    }
}
