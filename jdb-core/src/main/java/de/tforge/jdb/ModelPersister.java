package de.tforge.jdb;

import java.util.Map;

/**
 * Created by Tim Schiewe on 25.02.2017.
 */
public interface ModelPersister {
    Object persistNew(String tableName, String idColumn, Map<String, Object> columns) throws DatabaseException;
    void persistExisting(String tableName, String idColumn, Map<String, Object> columns) throws DatabaseException;
    void delete(String tableName, String idColumn, Map<String, Object> columns) throws DatabaseException;
}
