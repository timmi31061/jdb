package de.tforge.jdb;

import java.util.Map;

/**
 * Created by tim on 22.02.17.
 */
public abstract class Model {
    protected boolean isDirty = false;
    protected boolean exists = false;

    public abstract void adaptResult(Result r) throws DatabaseException;

    public abstract Map<String, Object> getAttributes() throws DatabaseException;

    public abstract void save() throws DatabaseException;
    public abstract void delete() throws DatabaseException;

    protected abstract String getIdColumnName();

    protected Object save(String tableName) throws DatabaseException {
        if(!isDirty) {
            return null;
        }

        isDirty = false;

        if(exists) {
            DatabaseManager.getModelPersister().persistExisting(tableName, getIdColumnName(), getAttributes());
        }
        else {
            return DatabaseManager.getModelPersister().persistNew(tableName, getIdColumnName(), getAttributes());
        }

        return null;
    }

    protected void delete(String tableName) throws DatabaseException {
        if(exists) {
            exists = false;
            DatabaseManager.getModelPersister().delete(tableName, getIdColumnName(), getAttributes());
        }
    }

    /*public String getTableName() {
        String name = getClass().getSimpleName();

        // UserConversationLabel => user_conversation_labels
        name = name.replaceAll("(?<!^)([A-Z])", "_$1").toLowerCase() + "s";

        return name;
    }*/
}
