package de.tforge.jdb;

import org.joda.time.DateTime;

/**
 * Created by tim on 22.02.17.
 */
public class OperatorBuilders {
    public static Operator equalTo(Integer val) {
        return DatabaseManager.getOperatorFactory().equalTo(val);
    }

    public static Operator equalTo(Long val) {
        return DatabaseManager.getOperatorFactory().equalTo(val);
    }

    public static Operator equalTo(Float val) {
        return DatabaseManager.getOperatorFactory().equalTo(val);
    }

    public static Operator equalTo(Double val) {
        return DatabaseManager.getOperatorFactory().equalTo(val);
    }

    public static Operator equalTo(Boolean val) {
        return DatabaseManager.getOperatorFactory().equalTo(val);
    }

    public static Operator equalTo(DateTime val) {
        return DatabaseManager.getOperatorFactory().equalTo(val);
    }

    public static Operator equalTo(String val) {
        return DatabaseManager.getOperatorFactory().equalTo(val);
    }


    public static Operator greaterThan(Integer val) {
        return DatabaseManager.getOperatorFactory().greaterThan(val);
    }

    public static Operator greaterThan(Long val) {
        return DatabaseManager.getOperatorFactory().greaterThan(val);
    }

    public static Operator greaterThan(Float val) {
        return DatabaseManager.getOperatorFactory().greaterThan(val);
    }

    public static Operator greaterThan(Double val) {
        return DatabaseManager.getOperatorFactory().greaterThan(val);
    }

    public static Operator greaterThan(DateTime val) {
        return DatabaseManager.getOperatorFactory().greaterThan(val);
    }


    public static Operator lessThan(Integer val) {
        return DatabaseManager.getOperatorFactory().lessThan(val);
    }

    public static Operator lessThan(Long val) {
        return DatabaseManager.getOperatorFactory().lessThan(val);
    }

    public static Operator lessThan(Float val) {
        return DatabaseManager.getOperatorFactory().lessThan(val);
    }

    public static Operator lessThan(Double val) {
        return DatabaseManager.getOperatorFactory().lessThan(val);
    }

    public static Operator lessThan(DateTime val) {
        return DatabaseManager.getOperatorFactory().lessThan(val);
    }


    public static Operator inArray(Object[] vals) {
        return DatabaseManager.getOperatorFactory().inArray(vals);
    }


    public static Operator not(Operator op) {
        return DatabaseManager.getOperatorFactory().not(op);
    }
}
