package de.tforge.jdb;

/**
 * Created by tim on 22.02.17.
 */
public enum OrderDirection {
    ASC, DESC
}
