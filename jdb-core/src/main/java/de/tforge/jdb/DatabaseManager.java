package de.tforge.jdb;

/**
 * Created by tim on 22.02.17.
 */
public class DatabaseManager {
    private static DatabaseFactory factory;

    public static DatabaseFactory getFactory() {
        return factory;
    }

    public static void setFactory(DatabaseFactory factory) {
        DatabaseManager.factory = factory;
    }

    public static <T extends Model> QueryBuilder<T> createQueryBuilder(Class<T> modelClass, String tableName) {
        return getFactory().createQueryBuilder(modelClass, tableName);
    }

    public static OperatorFactory getOperatorFactory() {
        return getFactory().getOperatorFactory();
    }

    public static TypeConverter getTypeConverter() {
        return getFactory().getTypeConverter();
    }

    public static ModelPersister getModelPersister() {
        return getFactory().getModelPersister();
    }

    private DatabaseManager() {
    }
}
