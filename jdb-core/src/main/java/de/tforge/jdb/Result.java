package de.tforge.jdb;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by tim on 23.02.17.
 */
public class Result extends Model {
    public class Entry {
        private final String type;
        private final Object value;

        public Entry(String type, Object value) {
            this.type = type;
            this.value = DatabaseManager.getTypeConverter().convertFromDB(value);
        }

        public String getType() {
            return type;
        }

        public Object getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.format("{%s: %s}", type, value.toString());
        }
    }

    private HashMap<String, Entry> columns = new HashMap<>();

    @Override
    public void adaptResult(Result r) {
        this.columns = r.columns;
    }

    @Override
    public Map<String, Object> getAttributes() {
        return Collections.unmodifiableMap(
                columns.entrySet().stream().collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue().getValue()))
        );
    }

    @Override
    public void save() {
        throw new NotImplementedException();
    }

    @Override
    public void delete() throws DatabaseException {
        throw new NotImplementedException();
    }

    @Override
    protected String getIdColumnName() {
        return null;
    }

    public void setColumn(String name, Entry value) {
        columns.put(name, value);
    }

    public Entry getColumn(String name) {
        return columns.get(name);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("{\n");

        for (Map.Entry<String, Entry> col : columns.entrySet()) {
            builder.append("    ");
            builder.append(col.getKey());
            builder.append(" -> ");
            builder.append(col.getValue().toString());
            builder.append("\n");
        }

        builder.append("}");
        return builder.toString();
    }
}
