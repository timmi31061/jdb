package de.tforge.jdb;

/**
 * Created by tim on 25.02.17.
 */
public interface TypeConverter {
    Object convertFromDB(Object value);
    Object convertToDB(Object value);
}
