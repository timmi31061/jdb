import de.tforge.jdb.DatabaseException;
import de.tforge.jdb.DatabaseManager;
import de.tforge.jdb.mysql.MySqlDatabaseFactory;
import org.joda.time.DateTime;
import org.junit.*;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;
import static de.tforge.jdb.OperatorBuilders.*;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by tim on 02.03.17.
 */
public class CrudMySqlTest {
    private Deal deal1;
    private Deal deal2;
    private Deal deal3;
    private Deal deal4;
    private Deal deal5;

    @BeforeClass
    public static void initialize() {
        // Connect to MySQL
        MySqlDatabaseFactory.setDatabase("jdbc:mariadb://tforge-root.tfl:3306/jdbtest?autoReconnect=true", "jdbtest", "jdbtest");
        DatabaseManager.setFactory(MySqlDatabaseFactory.getInstance());
    }

    @Before
    public void prepareTest() throws SQLException, DatabaseException {
        MySqlDatabaseFactory.getInstance().prepare(
                "CREATE TABLE `deals` (\n" +
                        "`dealID` bigint(20) NOT NULL PRIMARY KEY,\n" +
                        "  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,\n" +
                        "  `isBuy` tinyint(1) NOT NULL,\n" +
                        "  `amount` double NOT NULL,\n" +
                        "  `price` double NOT NULL\n" +
                        ") ENGINE=InnoDB").execute();

        MySqlDatabaseFactory.getInstance().prepare("INSERT INTO deals (dealID, time, isBuy, amount, price) VALUES (1, '2017-01-20 04:23:59', 1, 0.06679742, 900.12)").execute();
        MySqlDatabaseFactory.getInstance().prepare("INSERT INTO deals (dealID, time, isBuy, amount, price) VALUES (2, '2017-02-23 20:55:19', 0, 0.25, 1161.99)").execute();
        MySqlDatabaseFactory.getInstance().prepare("INSERT INTO deals (dealID, time, isBuy, amount, price) VALUES (3, '2017-02-24 01:32:24', 1, 1.54, 1187.82)").execute();
        MySqlDatabaseFactory.getInstance().prepare("INSERT INTO deals (dealID, time, isBuy, amount, price) VALUES (4, '2017-03-02 15:15:59', 1, 0.76, 1242.03)").execute();
        MySqlDatabaseFactory.getInstance().prepare("INSERT INTO deals (dealID, time, isBuy, amount, price) VALUES (5, '2017-03-02 16:12:38', 1, 0.41633, 1242.03)").execute();

        deal1 = new Deal(1);
        deal2 = new Deal(2);
        deal3 = new Deal(3);
        deal4 = new Deal(4);
        deal5 = new Deal(5);
    }

    @After
    public void cleanupTest() throws SQLException {
        MySqlDatabaseFactory.getInstance().prepare("DROP TABLE deals").execute();
    }

    private static void assertDealEquals(Deal d1, Deal d2) {
        assertEquals(d1.getDealID(), d2.getDealID());
        assertEquals(d1.getTime(), d2.getTime());
        assertEquals(d1.getIsBuy(), d2.getIsBuy());
        assertEquals(d1.getAmount(), d2.getAmount(), 0.00001);
        assertEquals(d1.getPrice(), d2.getPrice(), 0.00001);
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testCount() throws DatabaseException {
        long count = Deal.count();
        assertEquals(count, 5);
    }

    @Test
    public void testCreate() throws DatabaseException {
        Deal deal = new Deal();
        deal.setTime(new DateTime(2015, 1, 1, 12, 42, 10));
        deal.setIsBuy(true);
        deal.setAmount(152);
        deal.setPrice(684.64);
        deal.save();

        Deal actual = new Deal(deal.getDealID());
        assertDealEquals(deal, actual);
    }

    @Test
    public void testReadNonExistentModelFails() throws DatabaseException {
        thrown.expect(DatabaseException.class);
        thrown.expectMessage("Unable to find Deal #42");
        Deal deal = new Deal(42);
    }

    @Test
    public void testRead() throws DatabaseException {
        Deal deal = new Deal(3);
        assertDealEquals(deal, deal3);
    }

    @Test
    public void testUpdate() throws DatabaseException {
        Deal deal = new Deal(5);
        assertDealEquals(deal, deal5);
        deal.setAmount(124);
        deal.save();

        Deal actual = new Deal(deal.getDealID());
        assertDealEquals(deal, actual);
        deal5 = actual;
    }

    @Test
    public void testDelete() throws DatabaseException {
        Deal deal = new Deal(1);
        deal.delete();

        thrown.expect(DatabaseException.class);
        thrown.expectMessage("Unable to find Deal #1");
        Deal actual = new Deal(1);
    }


    @Test
    public void testOperatorEqualToLong() throws DatabaseException {
        Deal deal = Deal.where(Deal.DEAL_ID, equalTo(4)).first();
        assertDealEquals(deal, deal4);
    }

    @Test
    public void testOperatorEqualToDateTime() throws DatabaseException {
        Deal deal = Deal.where(Deal.TIME, equalTo(new DateTime(2017, 2, 23, 20, 55, 19))).first();
        assertDealEquals(deal, deal2);
    }

    @Test
    public void testOperatorEqualToBool() throws DatabaseException {
        Deal deal = Deal.where(Deal.IS_BUY, equalTo(false)).first();
        assertDealEquals(deal, deal2);
    }


    @Test
    public void testOperatorGreaterThanLong() throws DatabaseException {
        List<Deal> deals = Deal.where(Deal.DEAL_ID, greaterThan(3)).get().collect(Collectors.toList());
        assertEquals(2, deals.size());

        assertDealEquals(deal4, deals.get(0));
        assertDealEquals(deal5, deals.get(1));
    }

    @Test
    public void testOperatorGreaterThanDateTime() throws DatabaseException {
        List<Deal> deals = Deal.where(Deal.TIME, greaterThan(new DateTime("2017-02-23T20:55:19+01:00"))).get().collect(Collectors.toList());
        assertEquals(3, deals.size());

        assertDealEquals(deal3, deals.get(0));
        assertDealEquals(deal4, deals.get(1));
        assertDealEquals(deal5, deals.get(2));
    }


    @Test
    public void testOperatorLessThanLong() throws DatabaseException {
        List<Deal> deals = Deal.where(Deal.DEAL_ID, lessThan(3)).get().collect(Collectors.toList());
        assertEquals(2, deals.size());

        assertDealEquals(deal1, deals.get(0));
        assertDealEquals(deal2, deals.get(1));
    }

    @Test
    public void testOperatorLessThanDateTime() throws DatabaseException {
        DateTime time = new DateTime("2017-03-02T15:15:59+01:00");
        Timestamp ts = new Timestamp(time.getMillis());

        List<Deal> deals = Deal.where(Deal.TIME, lessThan(new DateTime("2017-03-02T15:15:59+01:00"))).get().collect(Collectors.toList());
        assertEquals(3, deals.size());

        assertDealEquals(deal1, deals.get(0));
        assertDealEquals(deal2, deals.get(1));
        assertDealEquals(deal3, deals.get(2));
    }

    @Test
    public void testOperatorNot() throws DatabaseException {
        Deal deal = Deal.where(Deal.DEAL_ID, not(greaterThan(1))).first();
        assertDealEquals(deal, deal1);
    }
}
